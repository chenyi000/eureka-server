package com.dt76.wmsEureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class server {

    public static void main(String[] args) {
        SpringApplication.run(server.class, args);
    }

}
